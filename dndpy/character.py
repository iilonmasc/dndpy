import math
from dndpy import constants


class Attribute():
    def __init__(self, bonus=0, proficiency=False, expertise=False):
        self.bonus = bonus
        self.proficiency = proficiency
        self.expertise = expertise


class Bonus():
    def __init__(self, field, name, value):
        self.id = name
        self.field = field
        self.value = value


class Character():

    def __init__(self):
        self.name = 'John Doe'
        self.race = 'Human'
        self.character_class = 'Monk'
        self.level = 1
        self.max_hp = 0
        self.current_hp = 0
        self.temporary_hp = 0
        self.max_hitdice = {
            'd4': {'amount': 0},
            'd6': {'amount': 0},
            'd8': {'amount': 0},
            'd10': {'amount': 0},
            'd12': {'amount': 0},
            'd20': {'amount': 0},
        }
        self.current_hitdice = {
            'd4': {'amount': 0},
            'd6': {'amount': 0},
            'd8': {'amount': 0},
            'd10': {'amount': 0},
            'd12': {'amount': 0},
            'd20': {'amount': 0},
        }
        self.special_resources = {'wild-shape': 0, 'ki': 0}
        self.spell_slots = {
            1: {'max': 0, 'used': 0},
            2: {'max': 0, 'used': 0},
            3: {'max': 0, 'used': 0},
            4: {'max': 0, 'used': 0},
            5: {'max': 0, 'used': 0},
            6: {'max': 0, 'used': 0},
            7: {'max': 0, 'used': 0},
            8: {'max': 0, 'used': 0},
            9: {'max': 0, 'used': 0},
        }
        self.currencies = {
            'copper': 0,
            'silver': 0,
            'electrum': 0,
            'gold': 0,
            'platinum': 0,
        }
        self.inventory = []
        self.experience = 0
        self.alignment = constants.ALIGNMENT.get('nn')
        self.proficiency_bonus = self.get_proficiency_bonus()
        self.stats = {'str': Stat(), 'dex': Stat(), 'con': Stat(),
                      'int': Stat(), 'wis': Stat(), 'cha': Stat()}
        self.skills = {}
        self.bonus = {}
        self.initialize_skillset()
        self.update_sheet()

    def add_bonus(self, bonus):
        if not self.bonus.get(bonus.field):
            self.bonus[bonus.field] = []
        self.bonus[bonus.field].append({'id': bonus.id, 'value': bonus.value})
        self.update_sheet()

    def add_expertise(self, skill):
        self.skills.get(skill).expertise = True
        if skill == 'perception':
            self.update_passive_wisdom()

    def add_proficiency(self, skill):
        self.skills.get(skill).proficiency = True
        if skill == 'perception':
            self.update_passive_wisdom()

    @classmethod
    def calculate_armor_class(cls, dexterity_bonus, additional_bonus=0):
        return 10 + dexterity_bonus + additional_bonus

    @classmethod
    def calculate_initiative(cls, dexterity_bonus, additional_bonus=0):
        return dexterity_bonus + additional_bonus

    def get_bonus(self, field):
        bonus_values = self.bonus.get(field)
        total_bonus = 0
        if bonus_values:
            for bonus in bonus_values:
                total_bonus += bonus.get('value')
        return total_bonus

    def get_proficiency_bonus(self):
        if self.level == 0 or self.level in range(1, 5):
            return 2
        elif self.level in range(5, 9):
            return 3
        elif self.level in range(9, 13):
            return 4
        elif self.level in range(13, 17):
            return 5
        elif self.level in range(17, 21) or self.level > 20:
            return 6

    def get_save_bonus(self, stat_key):
        stat = self.stats.get(stat_key)
        prof_bonus = self.get_proficiency_bonus()
        if stat.expertise:
            additional_bonus = prof_bonus*2
        elif stat.proficiency:
            additional_bonus = prof_bonus
        else:
            additional_bonus = 0
        return stat.modifier + additional_bonus

    def get_skill_bonus(self, skill_key):
        skill = self.skills.get(skill_key)
        prof_bonus = self.get_proficiency_bonus()
        if skill.expertise:
            additional_bonus = prof_bonus*2
        elif skill.proficiency:
            additional_bonus = prof_bonus
        else:
            additional_bonus = 0
        return skill.stat.modifier + additional_bonus

    def initialize_skillset(self):
        stat_dictionary = {'str': constants.STR_SKILLS, 'dex': constants.DEX_SKILLS,
                           'int': constants.INT_SKILLS, 'wis': constants.WIS_SKILLS, 'cha': constants.CHA_SKILLS}
        for stat_key in stat_dictionary:
            skillset = stat_dictionary.get(stat_key)
            stat = self.stats.get(stat_key)
            for skill_key in skillset:
                skill = Skill(skill_key, stat)
                self.skills[skill_key] = skill

    def level_up(self, levels=0):
        self.level += levels
        self.update_sheet()

    def remove_expertise(self, skill):
        self.skills.get(skill).expertise = False
        if skill == 'perception':
            self.update_passive_wisdom()

    def remove_proficiency(self, skill):
        self.skills.get(skill).proficiency = False
        if skill == 'perception':
            self.update_passive_wisdom()

    def update_passive_wisdom(self):
        self.passive_wisdom = 10 + \
            self.get_skill_bonus('perception') + \
            self.get_bonus('passive_wisdom')

    def update_sheet(self):
        self.max_hp = self.stats.get('con').modifier * self.level
        self.proficiency_bonus = self.get_proficiency_bonus()
        for stat_key in ['str', 'dex', 'con', 'int', 'wis', 'cha']:
            self.update_stat(stat_key, self.stats.get(stat_key))
        self.armor_class = Character.calculate_armor_class(
            self.stats.get('dex').modifier, self.get_bonus('armor_class'))
        self.initiative = Character.calculate_initiative(
            self.stats.get('dex').modifier, self.get_bonus('initiative'))
        self.update_passive_wisdom()

    def update_stat(self, stat_key, stat):
        bonus = self.get_bonus(stat_key)
        new_stat = Stat(
            stat.value, stat.proficiency, stat.expertise, bonus)
        new_stat.skills = self.stats[stat_key].skills
        for skill in new_stat.skills:
            new_stat.skills.get(skill).stat = new_stat
        self.stats[stat_key] = new_stat


class Skill(Attribute):
    def __init__(self, name, stat, proficiency=False, expertise=False, bonus=0):
        super().__init__(bonus, proficiency, expertise)
        self.name = name
        self.stat = stat
        stat.add_skill(self)


class Stat(Attribute):
    def __init__(self, value=0, proficiency=False, expertise=False, bonus=0):
        super().__init__(bonus, proficiency, expertise)
        self.value = value
        self.modifier = Stat.calculate_stat_modifier(self.value, self.bonus)
        self.skills = {}

    def add_skill(self, skill):
        self.skills[skill.name] = skill

    @classmethod
    def calculate_stat_modifier(cls, value, bonus=0):
        return math.floor((value + bonus - 10) / 2)
