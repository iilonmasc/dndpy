ALIGNMENT = {
    'lg': 'lawful good',
    'ln': 'lawful neutral',
    'le': 'lawful evil',
    'ng': 'neutral good',
    'nn': 'true neutral',
    'ne': 'neutral evil',
    'cg': 'chaotic good',
    'cn': 'chaotic neutral',
    'ce': 'chaotic evil',
}

STR_SKILLS = ['athletics']
DEX_SKILLS = ['acrobatics', 'sleight-of-hand', 'stealth']
INT_SKILLS = ['arcana', 'history', 'investigation', 'nature', 'religion']
WIS_SKILLS = ['animal-handling', 'insight',
              'medicine', 'perception', 'survival']
CHA_SKILLS = ['deception', 'intimidation', 'performance', 'persuasion']
