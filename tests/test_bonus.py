from dndpy.character import Character, Bonus, Stat


def test_initiative_bonus_calculation():
    character = Character()
    character.update_stat('dex', Stat(12))
    character.add_bonus(Bonus('initiative', 'monk bonus', 2))
    assert character.initiative == 3


def test_armor_class_bonus_calculation():
    character = Character()
    character.update_stat('dex', Stat(12))
    character.add_bonus(Bonus('armor_class', 'shield', 2))
    assert character.armor_class == 13


def test_passive_wisdom_bonus_calculation():
    character = Character()
    character.update_stat('wis', Stat(16))
    character.add_bonus(Bonus('passive_wisdom', 'alertness', 5))
    assert character.passive_wisdom == 18


def test_stat_bonus_calculation():
    character = Character()
    character.update_stat('dex', Stat(12))
    assert character.stats.get('dex').modifier == 1
    character.add_bonus(Bonus('dex', 'race bonus', 2))
    assert character.stats.get('dex').modifier == 2
