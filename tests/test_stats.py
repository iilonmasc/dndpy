from dndpy.character import Stat


def test_stat_creation():
    test_stat = Stat(12, True)
    assert test_stat.value == 12
    assert test_stat.modifier == 1
    assert test_stat.proficiency == True
    assert test_stat.expertise == False
    test_stat = Stat(9)
    assert test_stat.value == 9
    assert test_stat.modifier == -1
    assert test_stat.proficiency == False
    assert test_stat.expertise == False


def test_stat_saves():
    test_stat = Stat(9)
    assert test_stat.proficiency == False
    assert test_stat.expertise == False
    test_stat = Stat(9, True)
    assert test_stat.proficiency == True
    assert test_stat.expertise == False
    test_stat = Stat(9, True, True)
    assert test_stat.proficiency == True
    assert test_stat.expertise == True
    test_stat = Stat(9, False, True)
    assert test_stat.proficiency == False
    assert test_stat.expertise == True


def test_stat_modifier_calculation():
    assert Stat.calculate_stat_modifier(0) == -5
    assert Stat.calculate_stat_modifier(1) == -5
    assert Stat.calculate_stat_modifier(2) == -4
    assert Stat.calculate_stat_modifier(3) == -4
    assert Stat.calculate_stat_modifier(4) == -3
    assert Stat.calculate_stat_modifier(5) == -3
    assert Stat.calculate_stat_modifier(6) == -2
    assert Stat.calculate_stat_modifier(7) == -2
    assert Stat.calculate_stat_modifier(8) == -1
    assert Stat.calculate_stat_modifier(9) == -1
    assert Stat.calculate_stat_modifier(10) == 0
    assert Stat.calculate_stat_modifier(11) == 0
    assert Stat.calculate_stat_modifier(12) == 1
    assert Stat.calculate_stat_modifier(13) == 1
    assert Stat.calculate_stat_modifier(14) == 2
    assert Stat.calculate_stat_modifier(15) == 2
    assert Stat.calculate_stat_modifier(16) == 3
    assert Stat.calculate_stat_modifier(17) == 3
    assert Stat.calculate_stat_modifier(18) == 4
    assert Stat.calculate_stat_modifier(19) == 4
    assert Stat.calculate_stat_modifier(20) == 5
    assert Stat.calculate_stat_modifier(21) == 5
    assert Stat.calculate_stat_modifier(22) == 6
    assert Stat.calculate_stat_modifier(23) == 6
    assert Stat.calculate_stat_modifier(24) == 7
    assert Stat.calculate_stat_modifier(25) == 7
    assert Stat.calculate_stat_modifier(26) == 8
    assert Stat.calculate_stat_modifier(27) == 8
    assert Stat.calculate_stat_modifier(28) == 9
    assert Stat.calculate_stat_modifier(29) == 9
    assert Stat.calculate_stat_modifier(30) == 10
