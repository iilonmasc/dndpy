from dndpy.character import Stat, Character, Bonus


def test_empty_character_creation():
    character = Character()
    assert character.alignment == 'true neutral'
    assert character.level == 1
    assert character.experience == 0
    assert character.get_save_bonus('dex') == -5
    assert character.get_skill_bonus('athletics') == -5


def test_stealth_proficient_character():
    character = Character()
    character.add_proficiency('stealth')
    assert 'stealth' in character.skills
    assert character.get_skill_bonus('stealth') == -3


def test_stealth_expertise_character():
    character = Character()
    character.add_expertise('stealth')
    assert 'stealth' in character.skills
    assert character.get_skill_bonus('stealth') == -1


def test_skill_calculation():
    character = Character()
    stat = 'dex'
    skill = 'acrobatics'
    character.update_stat(stat, Stat(16))
    character.update_sheet()
    assert character.get_skill_bonus(skill) == 3
    character.add_proficiency(skill)
    assert character.skills.get(skill).proficiency == True
    assert character.get_skill_bonus(skill) == 5
    character.add_expertise(skill)
    assert character.skills.get(skill).expertise == True
    assert character.get_skill_bonus(skill) == 7


def test_armor_class_calculation():
    character = Character()
    character.update_stat('dex', Stat(12))
    character.update_sheet()
    assert character.armor_class == 11


def test_initiative_calculation():
    character = Character()
    character.update_stat('dex', Stat(16))
    character.update_sheet()
    assert character.initiative == 3


def test_passive_wisdom_calculation():
    character = Character()
    character.update_stat('wis', Stat(16))
    character.update_sheet()
    assert character.stats.get('wis').value == 16
    assert character.stats.get('wis').modifier == 3
    assert character.passive_wisdom == 13
    character.add_proficiency('perception')
    assert character.passive_wisdom == 15
    assert character.skills.get('perception').proficiency == True
    character.add_expertise('perception')
    assert character.passive_wisdom == 17
    assert character.skills.get('perception').expertise == True


def test_character_save_at_levels():
    character = Character()
    character.update_stat('dex', Stat(16, True))
    character.update_sheet()
    assert character.initiative == 3
    character.level = 6
    character.update_sheet()
    assert character.get_save_bonus('dex') == 6
    character.update_stat('dex', Stat(16, True, True))
    character.update_sheet()
    assert character.get_save_bonus('dex') == 9


def test_character_level_up():
    character = Character()
    character.update_stat('con', Stat(20))
    character.update_sheet()
    assert character.max_hp == 5
    character.level_up(1)
    assert character.level == 2
    assert character.max_hp == 10
    character.level_up(5)
    assert character.level == 7
    assert character.max_hp == 35
